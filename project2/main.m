close all
clear all
clc

global theta_m; theta_m = 2;

%% Simulation parameters

sim_seconds = 10;  % Seconds of simulation
sample_period = 0.0001;  % Sample period
time = 0:sample_period:sim_seconds;  % Vector of sample timesteps
N = length(time); % Number of samples


%% System simulation 

[~,state] = ode45(@Estimator,time ,[1,2,0,0,0]); % Dynamic system simulation

thetahat = [state(:,1),state(:,2)];
phi = [state(:,4),state(:,5)];
x = state(:,3);
xhat = zeros(length(phi),1);
for i = 1:length(phi)
    xhat(i) = thetahat(i,:)*phi(i,:)';
end
error = x - xhat;


figure()
subplot(3,1,1)
plot(time,x, 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$x$'}, 'Interpreter','latex','FontSize',14);
title('Real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,2)
plot(time,xhat, 'LineWidth', 1.5, 'color', [0.4940 0.1840 0.5560])
legend({'$\hat{x}$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,3)
plot(time,error, 'LineWidth', 1.5, 'color', [0.9290 0.6940 0.1250])
legend({'$Estimation$ $error$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
figure()
subplot(2,1,1)
plot(time, theta_m - thetahat(:,1), 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$x$'}, 'Interpreter','latex','FontSize',14);
title('Real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
hold on
line([0,sim_seconds],[1.5,1.5])
grid on
subplot(2,1,2)
plot(time,thetahat(:,2), 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$x$'}, 'Interpreter','latex','FontSize',14);
title('Real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
hold on
line([0,sim_seconds],[2,2])
grid on

function dsdt = Estimator(t,state)
    

    global theta_m;

    u = 3;
%     u = 3*cos(2*t);
    thetahat = [state(1);state(2)];
    x = state(3);
    phi = [state(4);state(5)];
    x_hat = thetahat'*phi;
    e = x-x_hat;
    gamma = 5;
    
    dthetahat = gamma*e*phi;
    
%     dxhatdt = thetahat'*phi;
    dphidt = -theta_m*phi + [x;u];
    a = 1.5;
    b = 2;
    
    dxdt = -a*x+b*u;
    dsdt = [dthetahat;dxdt;dphidt];
  
end
