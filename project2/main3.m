close all
clear all
clc

%% Simulation parameters

sim_seconds = 10;  % Seconds of simulation
sample_period = 0.0001;  % Sample period
time = 0:sample_period:sim_seconds;  % Vector of sample timesteps
N = length(time); % Number of samples


%% System simulation 

[~,state] = ode45(@Estimator,time ,[-0.4,-3.2,4.1,-1.9,0.8,1.2,0,0,0,0]); % Dynamic system simulation

    A_hat = [state(:,1),state(:,2),state(:,3),state(:,4)];
    B_hat = [state(:,5),state(:,6)];
    x = [state(:,7),state(:,8)];
    x_hat = [state(:,9),state(:,10)];

error = x - x_hat;


figure()
subplot(3,1,1)
plot(time,x(:,1), 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$x$'}, 'Interpreter','latex','FontSize',14);
title('Real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,2)
plot(time,x_hat(:,1), 'LineWidth', 1.5, 'color', [0.4940 0.1840 0.5560])
legend({'$\hat{x}$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,3)
plot(time,error(:,1), 'LineWidth', 1.5, 'color', [0.9290 0.6940 0.1250])
legend({'$Estimation$ $error$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
figure()
subplot(2,1,1)
plot(time, B_hat(:,1), 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$x$'}, 'Interpreter','latex','FontSize',14);
title('Real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
hold on
line([0,sim_seconds],[1,1])
grid on
subplot(2,1,2)
plot(time,B_hat(:,2), 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$x$'}, 'Interpreter','latex','FontSize',14);
title('Real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
hold on
line([0,sim_seconds],[1.4,1.4])
grid on

function dsdt = Estimator(t,state)


    u = 7.5*cos(3*t)+10*cos(2*t);
    
    A_hat = [state(1),state(2);state(3),state(4)];
    B_hat = [state(5),state(6)];
    x = [state(7);state(8)];
    x_hat = [state(9);state(10)];
    e = x-x_hat;
    
    dA_hat = (x_hat*e')';
    dB_hat = u*e;
    dx_hat = A_hat*x_hat + B_hat'*u;

    A = [-0.5,-3;4,-2];
    B = [1;1.4];
    
    dxdt = A*x+B*u;
    dsdt = [dA_hat(1,:)';dA_hat(2,:)';dB_hat;dxdt;dx_hat];
  
end
