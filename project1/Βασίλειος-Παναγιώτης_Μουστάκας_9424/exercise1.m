close all;
clear all;
clc;

    global m_global;
    global b_global;
    global k_global;

%% Simulation parameters

sim_seconds = 10;  % Seconds of simulation
sample_period = 0.1;  % Sample period
time = 0:sample_period:sim_seconds;  % Vector of sample timesteps
N = length(time); % Number of samples
u = 10*sin(3*time) + 5; % Input force

[t,y] = ode45(@dynamic_system,time,[0,0]); % Dynamic system simulation

%% Calculate regression vector

zeta = zeros(N,3); % Regression vector
p1=50; % Filter poles
p2=50;
lambda_1 = p1+p2;
lambda_2 = p1*p2;
lambda_s = [1 lambda_1 lambda_2];  % Filter

zeta(:,1) = lsim(tf([-1,0],lambda_s),y(:,1),time);
zeta(:,2) = lsim(tf(-1,lambda_s),y(:,1),time);
zeta(:,3) = lsim(tf(1,lambda_s),u,time);

%% Estimated system parameters

theta_l = inv(zeta'*zeta)*zeta'*y(:,1);
theta_0 = theta_l + [lambda_1;lambda_2;0];




m_est = 1/theta_0(3);
k_est = m_est*theta_0(2);
b_est = m_est*theta_0(1);

    m_global = m_est;
    b_global = b_est;
    k_global = k_est;


fprintf(['Calculated theta: [ ',num2str(theta_0(1)),' , ',num2str(theta_0(2)),' , ',num2str(theta_0(3)),' ]', '\n\n'])
fprintf(['Estimated system parameters:\n'])
fprintf(['m = ',num2str(m_est), ' Kg\n'])
fprintf(['b = ',num2str(b_est), ' Kg/s\n'])
fprintf(['k = ',num2str(k_est), ' Kg/s^2\n'])


[t_hat,y_hat] = ode45(@dynamic_system_model,time,[0,0]);

figure()
plot(t,y(:,1), 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
hold on;
plot(t,y_hat(:,1), 'LineWidth', 1.5, 'color',[0.9290 0.6940 0.1250]	)
title('System output $p_1 = 50, p_2=50$', 'Interpreter', 'latex' ,'FontSize',18)

xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on

%% Dynamic system simulation

function dydt = dynamic_system(t,y)
    
    m = 10;
    b = 0.3;
    k = 1.5;
    u = 10*sin(3*t) + 5;

    dydt = [y(2); -(k/m)*y(1) - (b/m)*y(2) + u/m];
  
end

function dydt = dynamic_system_model(t,y)
    
    global m_global;
    global b_global;
    global k_global;

    m = m_global;
    b = b_global;
    k = k_global;
    u = 10*sin(3*t) + 5;

    dydt = [y(2); -(k/m)*y(1) - (b/m)*y(2) + u/m];
  
end
