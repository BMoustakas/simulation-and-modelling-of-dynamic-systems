clear all;
close all;
clc;

global theta_0_VR;

%% Simulation parameters

sim_seconds = 4;  % Seconds of simulation
sample_period = 0.0001;  % Sample period
time = 0:sample_period:sim_seconds;  % Vector of sample timesteps
N = length(time); % Number of samples
[Vr,Vc] = v(time);  % Systems outputs

Vr(randi([1 length(Vr)],1,3)) = randi([30 80],1,3);        % Random inputs

V1 = 3*sin(2*time);  % System input

%% Calculate regression vector based on VR

zeta = zeros(N,3); % Regression vector
p1=10; % Filter poles
p2=1000;
lambda_1 = p1+p2;
lambda_2 = p1*p2;
lambda_s = [1 lambda_1 lambda_2];  % Filter

zeta(:,1) = lsim(tf([-1,0],lambda_s),Vr,time);
zeta(:,2) = lsim(tf(-1,lambda_s),Vr,time);
zeta(:,3) = lsim(tf(1,lambda_s),V1,time);

theta_l = (zeta'*zeta/N)\zeta'*Vr'/N;
theta_0_VR = theta_l + [lambda_1;lambda_2;0];

%% System simulation with estimated parameters based on VR

[t_vr,Vr_hat] = ode15s(@dynamic_system_Vr,time ,[2,0]); % Dynamic system simulation

%% Plots
for i = 1:length(Vr)
    if (Vr(i)>10)
        Vr(i) = Vr(i-1);
    end
end


figure()
subplot(3,1,1)
plot(t_vr,Vr, 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$V_{R}$'}, 'Interpreter','latex','FontSize',14);
title('$V_{R}$ real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,2)
plot(t_vr,Vr_hat(:,1), 'LineWidth', 1.5, 'color', [0.4940 0.1840 0.5560])
legend({'$\hat{V}_R$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,3)
plot(t_vr,Vr-Vr_hat(:,1)', 'LineWidth', 1.5, 'color', [0.9290 0.6940 0.1250])
legend({'$Estimation$ $error$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on

%% Estimate system parameters based on VR

fprintf(['\nCalculated theta: [ ',num2str(theta_0_VR(1)),' , ',num2str(theta_0_VR(2)),' , ',num2str(theta_0_VR(3)),' ]', '\n\n'])
fprintf('Estimated system parameters:\n')
fprintf(['1/RC = ',num2str(theta_0_VR(1)), ' \n'])
fprintf(['1/LC = ',num2str(theta_0_VR(2)), ' \n'])

%% System simulation with estimated parameters based on VR

function dydt = dynamic_system_Vr(t,y)
    
global theta_0_VR;

    V1 = 3*sin(2*t);
    dydt = [y(2); (-theta_0_VR(1)*y(2) - theta_0_VR(2)*y(1) + theta_0_VR(3)*V1) ];
  
end
