close all
clear all
clc

global theta_0_VC;

%% Simulation parameters

sim_seconds = 2;  % Seconds of simulation
sample_period = 0.0001;  % Sample period
time = 0:sample_period:sim_seconds;  % Vector of sample timesteps
N = length(time); % Number of samples
[Vr,Vc] = v(time);  % Systems outputs
V1 = 3*sin(2*time);  % System input
V1_dot= 6*cos(2*time);

%% Calculate regression vector based on VC

zeta = zeros(N,3); % Regression vector
p1=10; % Filter poles
p2=1000;
lambda_1 = p1+p2;
lambda_2 = p1*p2;
lambda_s = [1 lambda_1 lambda_2];  % Filter

zeta(:,1) = lsim(tf([-1,0],lambda_s),(Vc-2),time);
zeta(:,2) = lsim(tf(-1,lambda_s),(Vc-2),time);
zeta(:,3) = lsim(tf(1,lambda_s),V1_dot,time);

theta_l = (zeta'*zeta/N)\zeta'*Vc'/N;
theta_0_VC = theta_l + [lambda_1;lambda_2;0];

%% System simulation with estimated parameters based on Vc

[t_vc,Vc_hat] = ode15s(@dynamic_system_Vc,time ,[-2,0]); % Dynamic system simulation
Vc_hat = Vc_hat + 2;

%% Plots

figure()
subplot(3,1,1)
plot(t_vc ,Vc, 'LineWidth', 1.5, 'color', [0.6350 0.0780 0.1840]	)
legend({'$V_{C}$'}, 'Interpreter','latex','FontSize',14);
title('$V_{C}$ real value, estimation and estimation error','Interpreter','latex','FontSize',18);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,2)
plot(t_vc,Vc_hat(:,1), 'LineWidth', 1.5, 'color', [0.4940 0.1840 0.5560])
legend({'$\hat{V}_C$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on
subplot(3,1,3)
plot(t_vc,(Vc-Vc_hat(:,1)'), 'LineWidth', 1.5, 'color', [0.9290 0.6940 0.1250])
legend({'$Estimation$ $error$'}, 'Interpreter','latex','FontSize',14);
xlabel('time $[sec]$', 'Interpreter','latex','FontSize',12);
grid on

%% Estimate system parameters based on VC

fprintf(['Calculated theta: [ ',num2str(theta_0_VC(1)),' , ',num2str(theta_0_VC(2)),' , ',num2str(theta_0_VC(3)),' ]', '\n\n'])
fprintf('Estimated system parameters:\n')
fprintf(['1/RC = ',num2str(theta_0_VC(1)), ' \n'])
fprintf(['1/LC = ',num2str(theta_0_VC(2)), ' \n'])

%% System simulation with estimated parameters based on VC

function dydt = dynamic_system_Vc(t,y)
    
global theta_0_VC;
%     theta_0_VC = [ 10,1e7,10,0];

    V= 6*cos(2*t);
    dydt = [y(2); (-theta_0_VC(1)*y(2) - theta_0_VC(2)*(y(1)) + theta_0_VC(3)*V) ];
  
end
